﻿using UnityEngine;
using System.Collections;
using System;

public class Projectile : MonoBehaviour {

    [HideInInspector]
    public float direction;
    [HideInInspector]
    public Vector3 velocity;
    public float speed = 3.0f;

    public int constantDelay = 50;
    private DateTime lastEffect;

    public GameObject deathPrefab;

    public bool assignTail = true;

    public int projectileTrail = -1;

    public Color spellColor = Color.white;

    public float damage = 25.0f;

	// Use this for initialization
	void Start () {
        lastEffect = DateTime.Now;
	}
    
    public void target(Vector3 loc)
    {
        //velocity = (loc - transform.position).normalized;
        direction = Mathf.Atan2((loc.y - transform.position.y), (loc.x - transform.position.x));
        velocity = Vector3.zero;
        velocity.x = Mathf.Cos(direction);
        velocity.y = Mathf.Sin(direction);
    }

    private bool hasTail = false;
	// Update is called once per frame
	void Update () {
        if (assignTail)
        {
            if (!hasTail)
            {
                hasTail = true;
                EffectManager.instance.GiveObjectTrail(this.gameObject, this, projectileTrail); // Give ourselves a spell tail

                // Use this chance to also apply lighting if we want it
                if (Globals.useLighting)
                {
                    EffectManager.instance.GiveProjectileLight(this);
                }
            }
        }

        transform.rotation = Quaternion.Euler(0, 0, (direction * Mathf.Rad2Deg) - 90);
        transform.position = transform.position + (velocity * Time.smoothDeltaTime * speed);

        if ((DateTime.Now - lastEffect).TotalMilliseconds >= constantDelay)
        {
            lastEffect = DateTime.Now;

            //GameObject g = GameObject.Instantiate(constantEffect, transform.position, Quaternion.identity) as GameObject;
        }
	}

    public void Die()
    {
        // If we have a death prefab spawn it
        if (deathPrefab)
        {
            GameObject.Instantiate(deathPrefab, transform.position, transform.rotation);
        }

        // Finally die
        Destroy(this.gameObject);
    }

    void OnTriggerEnter2D(Collider2D c)
    {
        if (c.gameObject.layer == 8)
        {
            // Hit a tile
            Die();
        }

        if (c.gameObject.tag == "Enemy")
        {
            c.GetComponent<Enemy>().TakeDamage(damage, velocity);
            Die();
        }
    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.tag == "Enemy")
        {
            coll.gameObject.GetComponent<Enemy>().TakeDamage(damage, velocity);
            Die();
        }
    }
}
