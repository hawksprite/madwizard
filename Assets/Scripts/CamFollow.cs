﻿using UnityEngine;
using System.Collections;

public class CamFollow : MonoBehaviour {

    public Transform target;
    public Vector3 velocity;
    public float dampTime = 0.1f;

    public float zoomSpeed = 20.0f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 point = GetComponent<Camera>().WorldToViewportPoint(target.position);
        Vector3 delta = target.position - GetComponent<Camera>().ViewportToWorldPoint(new Vector3(0.5f, 0.5f, point.z)); //(new Vector3(0.5, 0.5, point.z));
        Vector3 destination = transform.position + delta;
        transform.position = Vector3.SmoothDamp(transform.position, destination, ref velocity, dampTime);

        Camera.main.orthographicSize -= Input.GetAxis("Mouse ScrollWheel") * zoomSpeed * Time.smoothDeltaTime;
	}
}
