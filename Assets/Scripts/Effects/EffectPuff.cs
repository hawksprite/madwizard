﻿using UnityEngine;
using System.Collections;
using System;



public class EffectPuff : MonoBehaviour {

    public float direction = 0;
    public float moveSpeed = 5.0f;
    public float rotationSpeed = 3.0f;

    public Vector3 velocity;

	// Use this for initialization
	void Start () {
        direction = UnityEngine.Random.Range(0, 360);
        velocity = Vector3.zero;
        velocity.x = Mathf.Cos(direction);
        velocity.y = Mathf.Sin(direction);
	}
	
	// Update is called once per frame
	void Update () {
        // Rotate and move our puff
        transform.position = transform.position + (velocity * moveSpeed * Time.smoothDeltaTime);
        transform.Rotate(new Vector3(0, 0, rotationSpeed * Time.smoothDeltaTime));

        // Slowly scale down to nothing
        transform.localScale -= new Vector3(2f * Time.smoothDeltaTime, 2f * Time.smoothDeltaTime, 0);
	}
}
