﻿using UnityEngine;
using System.Collections;

public class EffectManager : MonoBehaviour {

    //Here is a private reference only this class can access
    private static EffectManager _instance;

    //This is the public reference that other classes will use
    public static EffectManager instance
    {
        get
        {
            //If _instance hasn't been set yet, we grab it from the scene!
            //This will only happen the first time this reference is used.
            if (_instance == null)
                _instance = GameObject.FindObjectOfType<EffectManager>();
            return _instance;
        }
    }

    public GameObject[] trailLibrary;
    public GameObject[] spellLibrary;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void SpawnSpell(Player parent, bool randomDirection = false)
    {
        // Get the prefab for our given spell
        GameObject s = getSpell();

        // Determine the spell target for velocity
        Vector3 tar = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        if (randomDirection)
        {
            // If they want a random direction over ride the mouse position with a random direction
            tar = Camera.main.ScreenToWorldPoint(new Vector3(Random.Range(0, Screen.width), Random.Range(0, Screen.height), 0));
        }

        // Spawn it and apply info
        GameObject g = GameObject.Instantiate(s, parent.transform.position, Quaternion.identity) as GameObject;

        // The projectile can handle the rest
        g.GetComponent<Projectile>().target(tar);
    }

    public void GiveProjectileLight(Projectile parent)
    {
        GameObject lightHolder = new GameObject("s-lightholder");

        // Give it a hold parent script to ensure it's location
        //lightHolder.AddComponent<HoldParent>();

        Light l = (Light)lightHolder.AddComponent<Light>();

        l.color = parent.spellColor; // Apply the spell color to it

        // Zero out it's position and child it to the projectile
        lightHolder.transform.parent = parent.transform;
        lightHolder.transform.position = Vector3.zero;
        lightHolder.transform.rotation = Quaternion.identity;
        lightHolder.transform.localPosition = Vector3.zero;
    }

    public void GiveObjectTrail(GameObject target, Projectile parent, int trail = -1)
    {
        GameObject choice;

        // If they don't provide a specific trail id then choose a random one from the library
        if (trail == -1)
        {
            choice = getTrail();
        }
        else
        {
            choice = trailLibrary[trail];
        }

        // Spawn our new trail at the target
        GameObject t = (GameObject)Instantiate(choice, target.transform.position, target.transform.rotation);

        // Attach our soft attach script
        SoftAttach s = t.AddComponent<SoftAttach>() as SoftAttach;

        s.Attach(target.transform); // Attach it to our game object target
    }

    public GameObject getTrail() {
        // Return a random trail from the library
        return trailLibrary[Random.Range(0, trailLibrary.Length)];
    }

    public GameObject getSpell()
    {
        return spellLibrary[Random.Range(0, spellLibrary.Length)];
    }
}
