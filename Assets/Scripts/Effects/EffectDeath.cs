﻿using UnityEngine;
using System.Collections;
using System;

public class EffectDeath : MonoBehaviour {

    public int millisecondLife = 200;

    private DateTime born;

	// Use this for initialization
	void Start () {
        born = DateTime.Now;
	}
	
	// Update is called once per frame
	void Update () {
        if ((DateTime.Now - born).TotalMilliseconds >= millisecondLife)
        {
            GameObject.Destroy(this.gameObject);
        }
	}
}
