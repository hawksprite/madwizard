﻿using UnityEngine;
using System.Collections;

using DateTime = System.DateTime;
using Debug2 = UnityEngine.Debug;

public class Player : MonoBehaviour {

    //Here is a private reference only this class can access
    private static Player _instance;

    //This is the public reference that other classes will use
    public static Player instance
    {
        get
        {
            //If _instance hasn't been set yet, we grab it from the scene!
            //This will only happen the first time this reference is used.
            if (_instance == null)
                _instance = GameObject.FindObjectOfType<Player>();
            return _instance;
        }
    }


    Rigidbody2D r;

    public GameObject baseProjectile;

    public float moveVelocity = 5.0f;
    public float velocityDepth = 1.0f;
    public float skinWidth = 1.0f;
    public float checkDepth = 1.0f;
    public float frameDampening = 5.0f;
    public float motorDampening = 0.2f;

    public int singleDelay = 100;
    public int outburstDelay = 1000;

    public int outburstSize = 10;

    public bool allowSpells = true;

    private Vector3 volumeVelocity = Vector3.zero;

    private Vector3 st;

    // Some cool down percentage utilitys
    public float cdSingle
    {
        get
        {
            float has = (float)(DateTime.Now - lastSingleShot).TotalMilliseconds;
            float need = (float)singleDelay;

            return (has / need);
        }
    }

    public float cdOutburst
    {
        get
        {
            float has = (float)(DateTime.Now - lastOutburst).TotalMilliseconds;
            float need = (float)outburstDelay;

            return (has / need);
        }
    }

	// Use this for initialization
	void Start () {
        r = GetComponent<Rigidbody2D>();
        //currentTarget = transform.position;

        lastOutburst = DateTime.Now;
        lastSingleShot = DateTime.Now;

        _moveTo = transform.position;

        st = transform.localScale;
	}

    private DateTime lastSingleShot;
    public void SingleShot()
    {
        if ((DateTime.Now - lastSingleShot).TotalMilliseconds >= singleDelay)
        {
            lastSingleShot = DateTime.Now;
            EffectManager.instance.SpawnSpell(this);
        }
    }
    private DateTime lastOutburst;
    public void Outburst()
    {
        if ((DateTime.Now - lastOutburst).TotalMilliseconds >= outburstDelay)
        {
            lastOutburst = DateTime.Now;

            // For the given outburst size spawn that many spell with the random direction flag on.
            for (int i = 0; i < outburstSize; i++)
            {
                EffectManager.instance.SpawnSpell(this, true);
            } 
        }
    }

    private float _motorVelocityX = 0.0f;
    private float _motorVelocityY = 0.0f;
    void newAgeMovement(float delta)
    {
        Vector3 frameVelocity = Vector3.zero;

        if (Input.GetKey(KeyCode.A))
        {
            //frameVelocity -= new Vector3(velocityDepth, 0, 0);
            _motorVelocityX -= velocityDepth * delta;
        }

        if (Input.GetKey(KeyCode.D))
        {
            //frameVelocity += new Vector3(velocityDepth, 0, 0);
            _motorVelocityX += velocityDepth * delta;
        }

        if (Input.GetKey(KeyCode.W))
        {
            //frameVelocity += new Vector3(0, velocityDepth, 0);
            _motorVelocityY += velocityDepth * delta;
        }

        if (Input.GetKey(KeyCode.S))
        {
            //frameVelocity -= new Vector3(0, velocityDepth, 0);
            _motorVelocityY -= velocityDepth * delta;
        }

        frameVelocity = new Vector3(_motorVelocityX, _motorVelocityY, 0);

        _motorVelocityX = Mathf.Lerp(_motorVelocityX, 0.0f, motorDampening);
        _motorVelocityY = Mathf.Lerp(_motorVelocityY, 0.0f, motorDampening);

        if (_motorVelocityX > 0)
        {
            // Going right
            transform.localScale = st;
        }
        else
        {
            // Going left
            Vector3 t = st;
            t.x *= -1;
            transform.localScale = t;
        }

        applyPositionChange(frameVelocity * delta * moveVelocity);
    }

    private int _clearCount = 0;
    private bool _clear = false;
    private Vector3 _moveTo = Vector3.zero;
    bool _hadHit = false;
    void applyPositionChange(Vector3 c)
    {
        // Break down and try both it's x and y values as far as they'll go
        bool yNeg = false;
        bool xNeg = false;

        if (c.y < 0) { yNeg = true; }
        if (c.x < 0) { xNeg = true; }

        c.y = getDistanceToTile(new Vector3(0, c.y, 0));
        if (_hadHit) { c.y = 0; }

        c.x = getDistanceToTile(new Vector3(c.x, 0, 0));
        if (_hadHit) { c.x = 0; }

        if (yNeg) c.y *= -1;
        if (xNeg) c.x *= -1;

        // Now apply the position

        //float finalTest = getDistanceToTile(c);

       // if (!_hadHit)
        //{
            _moveTo = transform.position + c;
            _clear = true;
        //}
        
    }

    float getDistanceToTile(Vector3 c)
    {
        _hadHit = false;

        Vector3 theory = transform.position + c;
        Vector2 direction = (theory - transform.position);
        float changeDistance = Vector3.Distance(transform.position, theory);

        RaycastHit2D[] hit = Physics2D.RaycastAll(transform.position, direction, changeDistance + checkDepth);

        //4Debug2.Log(hit.Length);

        foreach (RaycastHit2D h in hit)
        {
            if (h.collider)
            {
                if (h.collider.gameObject.layer == 8)
                {
                    _hadHit = true;
                    return Vector3.Distance(h.point, transform.position) + skinWidth;
                    //Debug2.Log("Tile In Raycast");
                }
            }
        }

        return changeDistance;
    }

    Vector3 _lastVelocity = Vector3.zero;
	// Update is called once per frame
	void Update () {

        Globals.playerLocation = transform.position;

        if (allowSpells)
        {
            if (Input.GetMouseButtonDown(0))
            {
                // On shoot call for the effect manager to spawn us a spell
                SingleShot();
            }

            if (Input.GetKeyDown(KeyCode.Space))
            {
                Outburst();
            }
        }

        //Vector3 prior = transform.position;
        newAgeMovement(Time.smoothDeltaTime);
        transform.position = Vector3.MoveTowards(transform.position, _moveTo, frameDampening);

        //_lastVelocity = (prior - transform.position);
	}

    void FixedUpdate()
    {
        //newAgeMovement(Time.smoothDeltaTime);
    }
}
