﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class GameEndSnail : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.tag == "Player")
        {
            Application.LoadLevel(4);
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            Application.LoadLevel(4);
        }
    }

    /*
    [ContextMenu("Import")]
    public void f()
    {
        GameObject[] o = GameObject.FindGameObjectsWithTag("F1");

        foreach (GameObject g in o)
        {
            g.transform.parent = transform;
        }
    }*/
}
