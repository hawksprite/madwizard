﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

using DateTime = System.DateTime;

public class MainStateManager : MonoBehaviour {

    public Text gameDialog;
    public Text villagerCount;

    public int[] stateDelays;

    private bool thirdStateChange = false;
    private bool secondStateChange = false;
    private bool firstStateChange = false;
    private DateTime lastState;

    public int totalSnails = 8;

    public RectTransform killPanel;

    private Vector3 originP;

    public bool snailsFinished = false;

    public Transform endPortal;
    private Vector3 originO;

	// Use this for initialization
	void Start () {
        gameDialog.text = DialogManager.instance.getNextDialog();
        lastState = DateTime.Now;

        originP = killPanel.transform.localPosition;
        killPanel.transform.localPosition = new Vector3(9000, 9000, 0);

        originO = endPortal.transform.localPosition;
        endPortal.transform.localPosition = new Vector3(8000, 8000, 0);
	}
	
	// Update is called once per frame
	void Update () {

        if (!snailsFinished)
        {
            if (!firstStateChange)
            {
                if ((DateTime.Now - lastState).TotalSeconds >= stateDelays[0])
                {
                    firstStateChange = true;

                    lastState = DateTime.Now;

                    gameDialog.text = DialogManager.instance.getNextDialog();

                    // Display the kill counter
                    killPanel.transform.localPosition = originP;
                }
            }
            else
            {
                if (!secondStateChange)
                {
                    if ((DateTime.Now - lastState).TotalSeconds >= stateDelays[1])
                    {
                        secondStateChange = true;

                        lastState = DateTime.Now;

                        gameDialog.text = DialogManager.instance.getNextDialog();

                        // Unlock the players spell system, let them slaughter everyone!
                        Player.instance.allowSpells = true;
                    }
                }
                else
                {
                }
            }

            villagerCount.text = (totalSnails - Globals.snailsKilled).ToString() + "/" + totalSnails.ToString() + " snails left";

            if (Globals.snailsKilled >= totalSnails)
            {
                killPanel.transform.localPosition = new Vector3(9000, 9000, 0);
                // DOne with snails
                gameDialog.text = DialogManager.instance.getNextDialog();

                snailsFinished = true;

                lastState = DateTime.Now;
            }
        }
        else
        {
            if ((DateTime.Now - lastState).TotalSeconds >= stateDelays[2])
            {
                lastState = DateTime.Now;

                gameDialog.text = DialogManager.instance.getNextDialog();

                // Spawn the portal
                endPortal.transform.localPosition = originO;
            }
        }
	}
}
