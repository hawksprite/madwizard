﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HomePlayerDManager : MonoBehaviour {

    public Text dialog;

	// Use this for initialization
	void Start () {
        dialog.text = DialogManager.instance.getNextDialog();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.tag == "Player")
        {
            Application.LoadLevel(3);
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            Application.LoadLevel(3);
        }
    }
}
