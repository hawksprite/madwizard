﻿using UnityEngine;
using System.Collections;

public class DialogZoneTrigger : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    bool hasTrig = false;
    void trig()
    {
        if (hasTrig == false)
        {
            hasTrig = true;
            UnityEngine.Debug.Log("Triggered");
            Globals.lastLevelDialog = DialogManager.instance.getNextDialog();
        }
    }

    /*
    void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.tag == "Player")
        {
            trig();
        }
    }*/

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            trig();
        }
    }
}
