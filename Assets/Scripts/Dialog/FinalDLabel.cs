﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class FinalDLabel : MonoBehaviour {

    private Text t;

	// Use this for initialization
	void Start () {
        t = GetComponent<Text>();

        Globals.lastLevelDialog = DialogManager.instance.getNextDialog();
	}
	
	// Update is called once per frame
	void Update () {
        t.text = Globals.lastLevelDialog;
	}
}
