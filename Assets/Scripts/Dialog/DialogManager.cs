﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DialogManager : MonoBehaviour {

    private static DialogManager _instance;

    public static DialogManager instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<DialogManager>();

                //Tell unity not to destroy this object when loading a new scene!
                DontDestroyOnLoad(_instance.gameObject);
            }

            return _instance;
        }
    }

    void Awake()
    {
        if (_instance == null)
        {
            //If I am the first instance, make me the Singleton
            _instance = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            //If a Singleton already exists and you find
            //another reference in scene, destroy it!
            if (this != _instance)
                Destroy(this.gameObject);
        }
    }

    public bool isFinished = false;

    public string dialogScript = "script_01";
    public List<string> dialogLib;
    public int currentIndex = 0;

	// Use this for initialization
	void Start () {
	
	}
	
    
	// Update is called once per frame
	void Update () {
	
	}

    public string getNextDialog()
    {
        checkLibrary();

        if (!isFinished)
        {

            currentIndex++;

            if (dialogLib.Count > currentIndex)
            {
                // This is all just bad logic, but it works for now
                if (dialogLib.Count < (currentIndex + 1))
                {
                    isFinished = true;
                }

                return dialogLib[currentIndex];
            }
            else
            {
                isFinished = true;
            }
        }

        return ""; // Shouldn't hit this point, unless they're calling on a dead dialog system.
    }

    private bool hasReadLibrary = false;
    void checkLibrary()
    {
        if (!hasReadLibrary)
        {
            hasReadLibrary = true;

            TextAsset txt = (TextAsset)Resources.Load("Story/" + dialogScript, typeof(TextAsset));
            string buffer = txt.text;

            // Clear our lib
            dialogLib = new List<string>();

            // Go through each line of our buffer and push it into the lib
            string[] lines = buffer.Split('\n');
            foreach (string l in lines)
            {
                dialogLib.Add(l);
            }
        }
    }
}
