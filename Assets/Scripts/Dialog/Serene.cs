﻿using UnityEngine;
using System.Collections;

public class Serene : MonoBehaviour {

    float maxDistance = 0;

    Vector3 target;

	// Use this for initialization
	void Start () {
        target = GameObject.FindGameObjectWithTag("F2").transform.position;
	}
	
	// Update is called once per frame
	void Update () {
        if (fade)
        {
            float llll = 1.0f - (Vector3.Distance(Globals.playerLocation, target) / maxDistance);
            Color c = Color.Lerp(Camera.main.backgroundColor, Color.white, llll);
            Camera.main.backgroundColor = c;
        }
	}

    bool fade = false;
    bool d = false;
    void b()
    {
        if (!d){
            d = true;
            maxDistance = Vector3.Distance(Globals.playerLocation, target);
            fade = true;
        }
    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.tag == "Player")
        {
            b();
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            b();
        }
    }
}
