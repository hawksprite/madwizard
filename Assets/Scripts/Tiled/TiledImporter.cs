﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[ExecuteInEditMode]
public class TiledImporter : MonoBehaviour {

    public float tileWidth = 70;
    public float tileHeight = 70;

    public int tileCountX = 100;
    public int tileCountY = 100;

    public string dataName = "";

    public string tileSetName = "Set01";

    private List<GameObject> links;

    public bool parentTiles = false;

    public string test = "";

    public int[] ignoreTiles; 

	// Use this for initialization
	void Start () {
	
	}

    [ContextMenu("Import")]
    public void editorBuild()
    {
        // Used by the editor to call buildTiles based off dataName set in the inspector
        buildTiles(dataName);
    }

    public void buildTiles(string map)
    {
        links = new List<GameObject>();

        // Bake the library before each build
        bakeLibrary();

        TextAsset txt = (TextAsset)Resources.Load("maps/" + map, typeof(TextAsset));
        string buffer = txt.text;

        string[] lines = buffer.Split('\n');

        //tileCountX = lines.Length - 1;
        //tileCountY = lines.Length - 1;

        buffer = buffer.Replace('\n', ',');

        string[] bits = buffer.Split(',');

        for (int i = 0; i < bits.Length; i++)
        {
            // TODO : This whole prep, try catch is bad code and should be fixed at some point.
            string prep = bits[i].Trim();
            bool safe = false;
            int t = 0;
            try
            {
                t = int.Parse(prep);
                safe = true;
            }
            catch (System.Exception z)
            {
                safe = false;
            }

            if (safe)
            {
                bool shouldIgnore = false;

                for (int z = 0; z < ignoreTiles.Length; z++)
                {
                    if (ignoreTiles[z] == t)
                    {
                        shouldIgnore = true;
                    }
                }

                if (!shouldIgnore)
                {
                    // 0 should be a null tile
                    // Otherwise build a tile here
                    float x = i % tileCountX;
                    float y = i / tileCountY;

                    Vector2 spot = new Vector2(x * tileWidth, y * tileHeight * -1.0f);

                    GameObject g = GameObject.Instantiate(getTileLib(t), new Vector3(spot.x, spot.y, 0), Quaternion.identity) as GameObject;

                    if (parentTiles)
                    {
                        g.transform.parent = gameObject.transform;
                    }

                    // Call on the tiled setup
                    g.GetComponent<TiledTile>().Setup(t);

                    links.Add(g);
                }
            }
        }
    }

    [ContextMenu("Clean")]
    public void cleanBuild()
    {
        if (links.Count > 0)
        {
            for (int i = 0; i < links.Count; i++)
            {
                if (links[i])
                {
                    DestroyImmediate(links[i]);
                }
            }
        }
    }

    public GameObject getTileLib(int type)
    {
        // Try to find the given tile
        for (int i = 0; i < libID.Count; i++)
        {
            if (libID[i] == type)
            {
                return libOBJ[i];
            }
        }

        // If it wasn't found just return the first object in the original library
        return libOBJ[0];
    }

    /////////
    // Tile library related stuff
    private bool hasBakedLibrary = false;

    private List<int> libID = new List<int>();
    private List<GameObject> libOBJ = new List<GameObject>();

    private void bakeLibrary()
    {
        hasBakedLibrary = true;

        libID = new List<int>();
        libOBJ = new List<GameObject>();

        // Find all the prefabs in the given folder name for this maps set
        Object[] tileLibrary = Resources.LoadAll<GameObject>(tileSetName);

        // Foreach tile in the given library find it's ID and add it to our baked library
        for (int i = 0; i < tileLibrary.Length; i++)
        {
            GameObject g = tileLibrary[i] as GameObject;

            // Find it's sub components set ID
            int foundID = g.GetComponent<TiledTile>().tileID;
            // Add it to our baked library now
            libID.Add(foundID);
            libOBJ.Add(g);
        }
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
