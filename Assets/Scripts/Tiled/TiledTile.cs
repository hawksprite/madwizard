﻿using UnityEngine;
using System.Collections;

public class TiledTile : MonoBehaviour {

    public string tileName = "";

    public int tileID = 152;

    public int intended = -1;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void Setup(int intend)
    {
        // Set what the intended id for this tile was supposed to be
        intended = intend;
    }
}
