﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class IntroDialog : MonoBehaviour {

    public Text dialogLabel;

    public int dialogTurns = 3;

    public float fadeInSpeed = 0.05f;
    public float fadeOutSpeed = 0.05f;

    public GameObject canvas;
    private List<GameObject> children;

	// Use this for initialization
	void Start () {
        dialogLabel.text = DialogManager.instance.getNextDialog();

        // Find all children for graphical effects
        children = new List<GameObject>();

        for (int i = 0; i < canvas.transform.childCount; i++)
        {
            children.Add(canvas.transform.GetChild(i).gameObject);
        }

        // Set the alpha out so it starts in black.
        setChildAlpha(0.0f);
        hasFadedIn = false;

	}

    private bool hasFadedIn = false;
    private float currentAlpha = 0.0f;
    private int c = 0;

    private bool shouldFadeOut = false;
	// Update is called once per frame
	void Update () {

        if (!hasFadedIn) {
            currentAlpha = Mathf.Lerp(currentAlpha, 1.0f, fadeInSpeed);
            
            if (currentAlpha >= 0.98f)
            {
                currentAlpha = 1.0f;
                hasFadedIn = true;
            }

            setChildAlpha(currentAlpha);

        }

        if (Input.GetMouseButtonUp(0))
        {
            if (!shouldFadeOut)
            {
                // Once they click just kill the fade in animation
                currentAlpha = 1.0f;
                hasFadedIn = true;
                setChildAlpha(currentAlpha);

                c++;

                if (c < dialogTurns)
                {
                    // Apply the next dialog
                    dialogLabel.text = DialogManager.instance.getNextDialog();
                }
                else
                {
                    // Done
                    shouldFadeOut = true; // Flag for the fade out
                }
            }
        }

        if (shouldFadeOut)
        {
            currentAlpha = Mathf.Lerp(currentAlpha, 0.0f, fadeOutSpeed);

            if (currentAlpha <= 0.01f)
            {
                currentAlpha = 0.0f;

                Application.LoadLevel(2); // On fade compleition go ahead and load the first level
            }

            setChildAlpha(currentAlpha);
        }
	
    }

    void setChildAlpha(float a)
    {
        // Now fade in
        foreach (GameObject g in children)
        {
            Text i = g.GetComponent<Text>() as Text;

            Color o = i.color; o.a = a;
            i.color = o;
        }
    }
}
