﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UICooldownDisplay : MonoBehaviour {

    public Slider cd1;
    public Slider cd2;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        cd1.value = Player.instance.cdSingle;
        cd2.value = Player.instance.cdOutburst;
	}
}
