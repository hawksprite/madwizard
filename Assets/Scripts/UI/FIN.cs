﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class FIN : MonoBehaviour {

    Text t;

	// Use this for initialization
	void Start () {
        t = GetComponent<Text>();
        t.color = Color.white;
	}
	
	// Update is called once per frame
	void Update () {
        t.color = Color.Lerp(t.color, Color.black, 0.006f);
	}
}
