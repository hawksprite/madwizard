﻿using UnityEngine;
using System.Collections;

public class TitleAnimation : MonoBehaviour {

    public float minScale = 0.8f;
    public float maxScale = 1.4f;

    public float scaleSpeed = 2.5f;

    private float currentScale = 1.0f;
    private bool toMax = true;

	// Use this for initialization
	void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {

        if (toMax)
        {
            currentScale = Mathf.Lerp(currentScale, maxScale, scaleSpeed);

            if (Vector3.Distance(new Vector3(currentScale,0,0), new Vector3(maxScale,0,0)) <= 0.05f)
            {
                toMax = false;
            }
        }
        else
        {
            currentScale = Mathf.Lerp(currentScale, minScale, scaleSpeed);

            if (Vector3.Distance(new Vector3(currentScale, 0, 0), new Vector3(minScale, 0, 0)) <= 0.05f)
            {
                toMax = true;
            }
        }

        //transform.localScale = new Vector3(currentScale, currentScale, 0);
        transform.localPosition = new Vector3(0, currentScale, 0);
	}
}
