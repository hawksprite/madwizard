﻿using UnityEngine;
using System.Collections;
using DateTime = System.DateTime;

public class PassiveAI : MonoBehaviour {

    private Enemy e;
    private Rigidbody2D r;

    private Vector3 goTo;

    public float speed = 3.0f;

	// Use this for initialization
	void Start () {
        e = GetComponent<Enemy>();
        r = GetComponent<Rigidbody2D>();

        goTo = transform.position;
        last = DateTime.Now;
	}

    DateTime last;

	void Update () {
        if ((DateTime.Now - last).TotalSeconds >= 2)
        {
            last = DateTime.Now;

            int choice = Random.Range(0, 2);

            // 50/50 shot of picking a new target
            if (choice == 0)
            {
                goTo = transform.position + new Vector3(Random.Range(-2, 2), 0, 0);
            }
        }

        float distance = Vector3.Distance(transform.position, goTo);

        if (distance >= 0.1f)
        {
            transform.position = Vector3.MoveTowards(transform.position, goTo, speed * Time.smoothDeltaTime);
        }
	}
}
