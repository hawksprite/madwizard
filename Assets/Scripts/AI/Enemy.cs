﻿using UnityEngine;
using System.Collections;
using DateTime = System.DateTime;

public class Enemy : MonoBehaviour {

    public float health = 100.0f;

    public GameObject deathPrefab;
    public GameObject hitPrefab;

    private SpriteRenderer s;
    private Color oC;

    private Rigidbody2D r;

	// Use this for initialization
	void Start () {
        s = GetComponent<SpriteRenderer>();
        oC = s.color;

        r = GetComponent<Rigidbody2D>();
	}


    private DateTime damageTimer;
    private bool displayDamage = false;
	// Update is called once per frame
	void Update () {
        if (health <= 0)
        {
            Die();
        }

        if (displayDamage)
        {
            if ((DateTime.Now - damageTimer).TotalMilliseconds >= 600)
            {
                displayDamage = false;
                s.color = oC;
            }
        }
	}

    public void Die()
    {
        Globals.snailsKilled++;
        if (deathPrefab)
        {
            // If we have a death prefab drop it on us
            GameObject.Instantiate(deathPrefab, transform.position, transform.rotation);
        }
        GameObject.Destroy(this.gameObject);
    }

    public void TakeDamage(float d, Vector3 incomingVelocity)
    {
        displayDamage = true;
        damageTimer = DateTime.Now;

        s.color = Color.red;

        health -= d;

        r.AddForce(incomingVelocity);

        if (hitPrefab)
        {
            GameObject.Instantiate(hitPrefab, transform.position, transform.rotation);
        }
    }

}
