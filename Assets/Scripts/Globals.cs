﻿using UnityEngine;
using System.Collections;

public static class Globals  {
    public static Vector3 playerLocation;

    public static bool useLighting = true;

    public static int snailsKilled = 0;

    public static string lastLevelDialog = "";
}
