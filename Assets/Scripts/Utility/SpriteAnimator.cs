﻿using UnityEngine;
using System.Collections;
using DateTime = System.DateTime;

public class SpriteAnimator : MonoBehaviour {

    private SpriteRenderer s;

    public Sprite[] spriteSet;

    private int currentSprite = 0;

    public int animationSpeed = 300;

    private DateTime t;

	// Use this for initialization
	void Start () {
        s = GetComponent<SpriteRenderer>();
        t = DateTime.Now;
	}
	
	// Update is called once per frame
	void Update () {
        if ((DateTime.Now - t).TotalMilliseconds >= animationSpeed)
        {
            t = DateTime.Now;

            currentSprite++;

            if (currentSprite >= spriteSet.Length)
            {
                currentSprite = 0;
            }

            s.sprite = spriteSet[currentSprite];
        }
	}
}
