﻿using UnityEngine;
using System.Collections;
using System;

public class SoftAttach : MonoBehaviour {

    public float dampen = 1.0f;
    public Transform target;

    public int lifeBias = 2000;

    private bool hasAttached = false;
    private bool isLive = true;

    public Vector3 attachmentOffset = Vector3.zero;

    DateTime pepper;

    private ParticleSystem pSystem;

	// Use this for initialization
	void Start () {
	    // See if we have a particle system
        pSystem = GetComponent<ParticleSystem>();
	}

    public void Attach(Transform t)
    {
        target = t;
        hasAttached = true;
    }
	
	// Update is called once per frame
	void Update () {
        if (hasAttached)
        {
            if (isLive)
            {
                if (target)
                {
                    // target is still alive, follow it
                    transform.position = target.position + attachmentOffset + new Vector3(0, 0, 1); // Always be 1 "z" layer below our soft attached target
                    transform.rotation = target.rotation;
                }
                else
                {
                    isLive = false; // Target has died
                    pepper = DateTime.Now; // Mark the moment when we found out pepper has died.

                    if (pSystem)
                    {
                        // If we have a particle system, stop it's emitter
                        pSystem.enableEmission = false;
                    }
                }
            }
            else
            {
                if ((DateTime.Now - pepper).TotalMilliseconds >= lifeBias)
                {
                    GameObject.Destroy(this.gameObject);
                }
            }
        }
	}
}
